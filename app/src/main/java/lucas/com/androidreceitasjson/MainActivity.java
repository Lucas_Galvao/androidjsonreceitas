package lucas.com.androidreceitasjson;

import android.app.Activity;
import android.os.Bundle;


/*
    Criar um app que leia um conjunto de receitas de um arquivo de json.
    Com:
    * Titulo da receita.
    * Tempo de preparo.
    * Quantas pessoas serve.
    * Lista de Ingredientes.
    Utilizar GSON.
 */
public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
